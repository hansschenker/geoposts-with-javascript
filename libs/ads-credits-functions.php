<?php
// The source code packaged with this file is Free Software, Copyright (C) 2005 by
// Ricardo Galli <gallir at uib dot es>.
// It's licensed under the AFFERO GENERAL PUBLIC LICENSE unless stated otherwise.
// You can get copies of the licenses here:
// 		http://www.affero.org/oagpl.html
// AFFERO GENERAL PUBLIC LICENSE is also included in the file called "COPYING".



/*****
// banners and credits funcions: FUNCTIONS TO ADAPT TO YOUR CONTRACTED ADS AND CREDITS
*****/


if (preg_match('/joneame.net$/', get_server_name())) {
	$globals['joneame']  = true;
}

if (preg_match('/localhost$/', get_server_name())) {
	$globals['localhost']  = true;
}

function do_banner_top () {
    require('carga-cortos.php');
}

function do_banner_right() { // side banner A
	global $globals;

/*	if($globals['external_ads'] && $globals['ads']) {
		@include('ads/right.inc');
	}*/
	//include('ads/darriba.inc');

	echo '<div align="center">';
	/*echo '<a href="/jonevision.php" target="_blank"><img src="'.$globals['base_url'].'img/v2/jv-banner-mini.png"/></a>';
	echo '<br/><br/>';*/
	echo '<a href="http://catlink.eu/" target="_blank"><img src="'.$globals['base_url'].'img/v2/catlinkbn.png"/></a>';
	echo '</div>';
	echo '<br/><p>&nbsp;</p>';
}

function do_credits() {
	global  $globals;

	echo '<div class="footlegal">';
	echo '<ul id="legalese">';

	// IMPORTANT: links change in every installation, CHANGE IT!!
	// contact info
	if ($globals['joneame']) {
		echo '<li>Jonéame</li>';
		echo ' - ';
		echo '<li><a href="'.$globals['base_url'].'ayuda.php?id=legal">'._('condiciones legales').'</a> ';
		echo '<a href="'.$globals['base_url'].'ayuda.php?id=uso">'._('y de uso').'</a></li>';
		echo ' - ';
		echo '<li><a href="http://joneame.net/COPYING">'._('licencia').'</a>, <a href="https://bitbucket.org/jonarano/joneame/src">'._('descargar').'</a></li>';	
		echo ' - ';
		echo '<li><a href="http://www.famfamfam.com/lab/icons/silk/">'._('iconos silk').'</a></li>';
		echo ' - ';
                echo '<li><a href="'.$globals['base_url'].'ayuda.php?id=legal">'.('contacto').'</a></li>';
		echo ' - ';
                echo '<li><a href="'.$globals['base_url'].'credits.php">'.('créditos').'</a></li>';
	} else {
		echo '<li>link to code and licenses here (please respect the menéame Affero license and publish your own code!)</li>';
		echo '<li><a href="">contact here</a></li>';
		echo '<li>code: <a href="#">Affero license here</a>, <a href="#">download code here</a></li>';
		echo '<li>you and contact link here</li>';
	}
	echo '</ul>'."\n";
	echo '</div>'."\n";

}
?>
