<?
// The source code packaged with this file is Free Software, Copyright (C) 2005 by
// Ricardo Galli <gallir at uib dot es>.
// It's licensed under the AFFERO GENERAL PUBLIC LICENSE unless stated otherwise.
// You can get copies of the licenses here:
// 		http://www.affero.org/oagpl.html
// AFFERO GENERAL PUBLIC LICENSE is also included in the file called "COPYING".

include('../config.php');

header('Content-Type: text/html; charset=UTF-8');
header('Pragma: no-cache');
header('Cache-Control: max-age=10, must-revalidate');
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';

$width = intval($_GET['width']);
$height = intval($_GET['height']);
$format = clean_input_string($_GET['format']);
$color_border = get_hex_color($_GET['color_border']);
$color_bg = get_hex_color($_GET['color_bg']);
$color_link = get_hex_color($_GET['color_link']);
$color_text = get_hex_color($_GET['color_text']);
$font_pt = is_numeric($_GET['font_pt']) ? floatval($_GET['font_pt']) : 10;
global $globals;

echo '<html><head><title>banner</title></head><body style="background: url(\''.$globals['base_url'].'img/estructura/corto.1.png\') -2px 0;">';
$res = $db->get_row("select link_id, link_title, count(*) as votes from links, votes where vote_type='links' and vote_date > date_sub(now(), interval 10 minute) and vote_value > 0 and link_id = vote_link_id group by link_id order by votes desc limit 1");
if ($res) {
	$votes_hour = $res->votes*6;
	$title['most'] = text_to_summary($res->link_title, 50) . ' <span style="font-size: 90%;">['.$votes_hour."&nbsp;"._('joneos por hora')."]</span>";
	$url['most'] = "http://".get_server_name()."/historia.php?id=$res->link_id";
}

$res = $db->get_row("select link_id, link_title, link_votes, link_anonymous from links where link_status = 'published' order by link_date desc limit 1");
if ($res) {
	$title['published'] = text_to_summary($res->link_title, 50) . ' <span style="font-size: 90%;">['.($res->link_votes+$res->link_anonymous)."&nbsp;"._('votos')."]</span>";
	$url['published'] = "http://".get_server_name()."/historia.php?id=$res->link_id";
}

$res = $db->get_row("select user_id, user_login, user_date, user_level,user_validated_date  from users where user_level != 'disabled' AND user_validated_date IS NOT NULL order by user_date desc limit 1");
if ($res) {
	$title['registrado'] = text_to_summary($res->user_login, 50) . ' <span style="font-size: 90%;">['._('¡Y le damos la bienvenida a la mafia!')."]</span> ";
	$url['registrado'] = "http://".get_server_name()."/mafioso.php?login=$res->user_login";
}

$res = $db->get_row("select link_id, link_title, link_votes, link_anonymous from links where link_status = 'queued' order by link_date desc limit 1");
if ($res) {
	$title['sent'] = text_to_summary($res->link_title, 70) . ' <span style="font-size: 90%;">['.($res->link_votes+$res->link_anonymous)."&nbsp;"._('votos')."]</span>";
	$url['sent'] = "http://".get_server_name()."/historia.php?id=$res->link_id";
}

$res = $db->get_row("select link_id, link_title, link_votes, link_anonymous from links, comments where link_id = comment_link_id  order by comment_id desc limit 1");

if ($res) {
	$title['commented'] = text_to_summary($res->link_title, 70) . ' <span style="font-size: 90%;">['.($res->link_votes+$res->link_anonymous)."&nbsp;"._('votos')."]</span>";
	$url['commented'] = "http://".get_server_name()."/historia.php?id=$res->link_id";
}

switch ($format) {
	case 'vertical':
		$div1 = '<div style="padding: 1px 1px 1px 1px; height: 23%; width: 100%; ">';
		$div2 = '<div style="padding: 1px 1px 1px 1px; height: 23%; width: 100%; border-top: 1px solid #'.$color_border.';">';
		$div3 = '<div style="padding: 1px 1px 1px 1px; height: 23%; width: 100%; border-top: 1px solid #'.$color_border.';">';
		$signature = _('Jonéame');
		break;
	case 'horizontal':
	default:
		$div1 = '<div style="position: absolute; left: 2px; top: 2px; width: 32%;">';
		$div2 = '<div style="position: absolute; left: 33%; top: 2px; width: 32%;">';
		$div3 = '<div style="position: absolute; left: 66%; top: 2px; width: 32%;">';
		$signature = _('Jonéame');
}

/*<?echo $div3;?>
<a href="<? echo $url['most']?>" style="color: #<? echo $color_link;?>" target="_parent"><? echo _('Eau de Jon'); ?></a><br />
<? echo $title['most'] ?>
</div>*/

?>
<div style="padding: 0 0 0 0 ; font-family: Verdana, Arial, sans-serif ; font-size: <? echo $font_pt;?>pt; line-height: 1.1em; color : #<? echo $color_text;?>; width: <? echo $width-2; ?>px; height: 
<? echo $height-2; ?>px; ">

<?echo $div1;?>
<a href="<? echo $url['published']?>" style="color: #<? echo $color_link;?>" target="_parent"><? echo _('&Uacute;ltima publicaci&oacute;n'); ?></a><br />
<? echo $title['published'] ?>
</div>

<?echo $div2;?>
<a href="<? echo $url['sent']?>" style="color: #<? echo $color_link;?>" target="_parent"><? echo _('&Uacute;ltima chorrada'); ?> </a><br />
<? echo $title['sent'] ?>
</div>

<? /*
<?echo $div4;?>
<a href="<? echo $url['commented']?>" style="color: #<? echo $color_link;?>" target="_parent"><? echo _('Desesperaci&oacute;n'); ?></a><br />
<? echo $title['commented'] ?>
</div>
*/ ?>

<?echo $div3;?>
<a href="<? echo $url['registrado']?>" style="color: #<? echo $color_link;?>" target="_parent"><? echo _('&Uacute;ltimo registrado'); ?></a><br />
<? echo $title['registrado'] ?>
</div>



<div style="position: absolute; left: 0; bottom: 0px; font-size: 8pt; height: 10pt; width: 100%; text-align: right;">
<a href="http://<?echo get_server_name();?>" style="color :transparent; background:transparent; text-decoration: none" target="_parent" ><?echo $signature;?></a>&nbsp;
</div>
</div>

<?
echo '</body></html>';
?>
