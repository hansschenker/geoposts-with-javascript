<?php

include '../config.php';
include mnminclude.'link.php';

$link = new Link;
$link->id = intval($_REQUEST['id']);

$new_link = $db->get_row("SELECT link_id, link_votes, link_aleatorios_positivos, link_anonymous, link_negatives, link_karma FROM links WHERE link_id=".$link->id);

$link->votes = $new_link->link_votes;
$link->anonymous = $new_link->link_anonymous;
$link->negatives = $new_link->link_negatives;
$link->karma = $new_link->link_karma;
$link->aleatorios_positivos = $new_link->link_aleatorios_positivos;

echo $link->json_votes_info();
