<?
// The source code packaged with this file is Free Software, Copyright (C) 2005 by
// Ricardo Galli <gallir at uib dot es>.
// It's licensed under the AFFERO GENERAL PUBLIC LICENSE unless stated otherwise.
// You can get copies of the licenses here:
// 		http://www.affero.org/oagpl.html
// AFFERO GENERAL PUBLIC LICENSE is also included in the file called "COPYING".

include('config.php');
include(mnminclude.'html1.php');
include(mnminclude.'sneak.php');

// Warning, it redirects to the content of the variable
if (!empty($globals['lounge_cotillona'])) {
	header('Location: http://'.get_server_name().$globals['base_url'].$globals['lounge_cotillona']);
	die;
}

$globals['activado_arriba'] = 1;
//$globals['ads'] = true;
$globals['favicon'] = 'img/favicons/favicon-coti.png';
$razon_baneo = "Has sido baneado de la cotillona de Jon&eacute;ame. Si piensas que hubo alg&uacute;n error, comun&iacute;canoslo a trav&eacute;s de <strong>admin@joneame.net</strong>";
init_sneak();

// Bana jaso bada prozesatu.
function ezabatu_uid_tablan($id, $tabla) {
global $db;
			$sqldel = "DELETE FROM $tabla WHERE uid='".$db->escape($id)."'";
			if($db->query($sqldel)) return true;
			return false;
}

if (!empty($_REQUEST['ban']))
{
// datubasin sartu te listo, bakarrik god bara.
if ($current_user->user_id > 0 && ($current_user->admin)) {
	$db->query("INSERT INTO fisban VALUES (NULL, '".$razon_baneo."','".$db->escape($current_user->user_id)."', '".$db->escape($_REQUEST['ban'])."')");
}

}

if (!empty($_REQUEST['unban']))
{
// datubasin sartu te listo, bakarrik god bara.
if ($current_user->user_id > 0 && ($current_user->admin) ) {
	ezabatu_uid_tablan($_REQUEST['unban'], 'fisban');
}

}

// Start html
if (!empty($_REQUEST['friends'])) {
	do_header(_('Amigos | Jon&eacute;ame'));
} elseif ($current_user->user_id > 0 && !empty($_REQUEST['admin']) && ($current_user->admin)) {
	do_header(_('Admin | Jon&eacute;ame'));
} elseif ($current_user->user_id > 0 && !empty($_REQUEST['devel']) && ($current_user->devel)) {
	do_header(_('Developers | Jon&eacute;ame'));
	
} else {
	do_header(_('Cotillona Móvil | Jon&eacute;ame'));
}

?>

<link rel="stylesheet" type="text/css" media="screen" href="<? echo $globals['base_url']; ?>css/cotillona.css" />

<script type="text/javascript">
//<![CDATA[
var my_version = '<? echo $sneak_version; ?>';
var ts=<? echo (time()-3600); ?>; // just due a freaking IE cache problem
var server_name = '<? echo get_server_name(); ?>';
var base_url = '<? echo $globals['base_url'];?>';
var sneak_base_url = 'http://'+'<? echo get_server_name().$globals['base_url'];?>'+'backend/cotillona.php';
var mykey = <? echo rand(100,999); ?>;
var is_admin = <? if ($current_user->admin) echo 'true'; else echo 'false'; ?>;
var is_devel = <? if ($current_user->devel) echo 'true'; else echo 'false'; ?>;


var default_gravatar =  'http://'+server_name+'/img/v2/no-avatar-20.png';
var do_animation = true;
var animating = false;
var animation_colors = Array("#5a8cbe", "#6a97c4", "#7ba3cb", "#8baed1", "#9cbad8", "#acc5de", "#bdd1e5", "#ceddec", "#dee8f2", "#eff4f9", "transparent");
var colors_max = animation_colors.length - 1;
var current_colors = Array();
var animation_timer;

var do_hoygan = <? if (isset($_REQUEST['hoygan']))  echo 'true'; else echo 'false'; ?>;
var do_flip = <? if (isset($_REQUEST['flip']))  echo 'true'; else echo 'false'; ?>;



// Reload the mnm banner each 5 minutes
var mnm_banner_reload = 180000;


$(function(){
start_sneak();
});

function play_pause() {
	if (is_playing()) {
		document.getElementById('play-pause-img').className = "icon play";
		if( document.getElementById('comment-input'))
			document.getElementById('comment-input').disabled=true;
		do_pause();
		
	} else {
		document.getElementById('play-pause-img').className = "icon pause";
		if (document.getElementById('comment-input'))
			document.getElementById('comment-input').disabled=false;
		do_play();
	}
	return false;

}

function set_initial_display(item, i) {
	var j;
	if (i >= colors_max)
		j = colors_max - 1;
	else j = i;
	current_colors[i] = j;
	item.css('background', animation_colors[j]);
}

function clear_animation() {
	clearInterval(animation_timer);
	animating = false;
	$('#items').children().css('background', 'transparent');
}

function animate_background() {
	if (current_colors[0] == colors_max) {
		clearInterval(animation_timer);
		animating = false;
		return;
	}
	var items = new Object; // For IE6
	items = $('#items').children();
	for (i=new_items-1; i>=0; i--) {
		if (current_colors[i] < colors_max) {
			current_colors[i]++;
			items.slice(i,i+1).css('background', animation_colors[current_colors[i]]);
		} else 
			new_items--;
	}
}


function to_html(data) {
	if (data.type != 'post' && data.type != 'chat') return 'undefined';

	var tstamp=new Date(data.ts*1000);
	var timeStr;
	var text_style = '';
	var chat_class = 'usneaker-chat';

	var hours = tstamp.getHours();
	var minutes = tstamp.getMinutes();
	var seconds = tstamp.getSeconds();

	timeStr  = ((hours < 10) ? "0" : "") + hours;
	timeStr  += ((minutes < 10) ? ":0" : ":") + minutes;
	// timeStr  += ((seconds < 10) ? ":0" : ":") + seconds;

	html = '<div class="usneaker-ts">['+timeStr+']<\/div>';

	switch (data.type) {
		case 'post':
			html += '<div class="usneaker-who">';
			html += '&nbsp;&lt;<a target="_blank" href="'+base_url+'mafioso.php?login='+data.who+'">'+data.who.substring(0,15)+'<\/a>&gt;<\/div>';
			if (check_user_ping(data.title)) {
				text_style = 'style="font-weight: bold;"';
			}
			if (do_hoygan) data.title = to_hoygan(data.title);
			if (do_flip) data.title = flipString(data.title);
			html += '<div class="usneaker-story" '+text_style+'><strong>[NOTITA]</strong>&nbsp;<a target="_blank" href="'+data.link+'">'+data.title+'<\/a><\/div>';

			return html;
			break;
		case 'chat':
			html += '<div class="usneaker-who">';
			html += '&nbsp;&lt;<a target="_blank" href="'+base_url+'mafioso.php?login='+data.who+'">'+data.who.substring(0,15)+'<\/a>&gt;<\/div>';
			if (check_user_ping(data.title)  || (is_admin && data.status != 'il capo' && check_admin_ping(data.title))) {
				text_style += 'font-weight: bold;';
			}
			if (global_options.show_admin || data.status == 'il capo') {
				chat_class = 'usneaker-chat-admin';
			} else if ( global_options.show_devel || data.status == '<? echo _('devel'); ?>') {  //aqui para editar el estado<<<<<<<
				chat_class = 'usneaker-chat-devel';
			} else if (global_options.show_friends || data.status == '<? echo _('cosa nostra'); ?>') {
				chat_class = 'usneaker-chat-friends';
			}  
			if (text_style.length > 0) {
				// Put the anchor in the same color as the rest of the text
				data.title = data.title.replace(/ href="/gi, ' style="'+text_style+'" href="');
				text_style = 'style="'+text_style+'"';
			}
			// Open in a new window
			data.title = data.title.replace(/(href=")/gi, 'target="_blank" $1'); 
			if (do_hoygan) data.title = to_hoygan(data.title);
			if (do_flip) data.title = flipString(data.title);
			html += '<div class="'+chat_class+'" '+text_style+'>';
			if (global_options.show_admin || data.status == 'il capo') {
				html += '##';
			} else if ( global_options.show_devel || data.status == '<? echo _('devel'); ?>') {  //aqui para editar el estado<<<<<<<
				html += '%%'; 
			} else if (global_options.show_friends || data.status == '<? echo _('cosa nostra'); ?>') {  //aqui para editar el estado<<<<<<<
				html += '@@';
			}  
			html += data.title+'<\/div>';

			return html;
			break;
	}

	return;

	html += '<div class="usneaker-votes" onmouseout="tooltip.clear(event);" onmouseover="tooltip.clear(event);">'+data.votes+'/'+data.com+'<\/div>';
	if ("undefined" != typeof(data.cid) && data.cid > 0) anchor='#c-'+data.cid;
	else anchor='';
	if (do_hoygan) data.title = to_hoygan(data.title);
	if (do_flip) data.title = flipString(data.title);
	html += '<div class="usneaker-story"><a target="_blank" href="'+data.link+anchor+'">'+data.title+'<\/a><\/div>';

	return html;
}


function check_user_ping(str) {
    if (user_login != '') {
        re = new RegExp('(^|[\\s:,\\?¿!¡;<>\\(\\)])'+user_login+'([\\s:,\\?¿!¡;<>\\(\\).]|$)', "i");
        return str.match(re);
    }
    return false;
}

function check_admin_ping(str) {
    re = new RegExp('(^|[\\s:,\\?¿!¡;<>\\(\\)])(admin|admins|administradora{0,1}|administrador[ae]s)([\\s:,\\?¿!¡;<>\\(\\).]|$)', "i");
    return str.match(re);
}

function to_hoygan(str) 
{
	str=str.replace(/á/gi, 'a');
	str=str.replace(/é/gi, 'e');
	str=str.replace(/í/gi, 'i');
	str=str.replace(/ó/gi, 'o');
	str=str.replace(/ú/gi, 'u');

	str=str.replace(/yo/gi, 'io');
	str=str.replace(/m([pb])/gi, 'n$1');
	str=str.replace(/qu([ei])/gi, 'k$1');
	str=str.replace(/ct/gi, 'st');
	str=str.replace(/cc/gi, 'cs');
	str=str.replace(/ll([aeou])/gi, 'y$1');
	str=str.replace(/ya/gi, 'ia');
	str=str.replace(/yo/gi, 'io');
	str=str.replace(/g([ei])/gi, 'j$1');
	str=str.replace(/^([aeiou][a-z]{3,})/gi, 'h$1');
	str=str.replace(/ ([aeiou][a-z]{3,})/gi, ' h$1');
	str=str.replace(/[zc]([ei])/gi, 's$1');
	str=str.replace(/z([aou])/gi, 's$1');
	str=str.replace(/c([aou])/gi, 'k$1');

	str=str.replace(/b([aeio])/gi, 'vvv;$1');
	str=str.replace(/v([aeio])/gi, 'bbb;$1');
	str=str.replace(/vvv;/gi, 'v');
	str=str.replace(/bbb;/gi, 'b');

	str=str.replace(/oi/gi, 'oy');
	str=str.replace(/xp([re])/gi, 'sp$1');
	str=str.replace(/es un/gi, 'esun');
	str=str.replace(/(^| )h([ae]) /gi, '$1$2 ');
	str=str.replace(/aho/gi, 'ao');
	str=str.replace(/a ver /gi, 'haber ');
	str=str.replace(/ por /gi, ' x ');
	str=str.replace(/ñ/gi, 'ny');
	str=str.replace(/buen/gi, 'GÜEN');

        // benjami
	str=str.replace(/windows/gi, 'güindous');
	str=str.replace(/we/gi, 'güe');
	// str=str.replace(/'. '/gi, '');
	str=str.replace(/,/gi, ' ');
	str=str.replace(/hola/gi, 'ola');
	str=str.replace(/ r([aeiou])/gi, ' rr$1');
	return str.toUpperCase();
}

// From http://www.revfad.com/flip.html
function flipString(aString) {
	aString = aString.toLowerCase();
	var last = aString.length - 1;
	var result = "";
	for (var i = last; i >= 0; --i) {
		result += flipChar(aString.charAt(i))
	}
	return result;
}

function flipChar(c) {
	switch (c) {
	case 'á':
	case 'a':
	case 'à':
		return '\u0250';
	case 'b':
		return 'q';
	case 'c':
		return '\u0254'; //Open o -- copied from pne
	case 'd':
		return 'p';
	case 'e':
	case 'é':
		return '\u01DD';
	case 'f':
		return '\u025F'; //Copied from pne -- 
		//LATIN SMALL LETTER DOTLESS J WITH STROKE
	case 'g':
		return 'b';
	case 'h':
		return '\u0265';
	case 'i':
	case 'í':
		return '\u0131'; //'\u0131\u0323' //copied from pne
	case 'j':
		return '\u0638';
	case 'k':
		return '\u029E';
	case 'l':
		return '1';
	case 'm':
		return '\u026F';
	case 'n':
	case 'ñ':
		return 'u';
	case 'ó':
	case 'o':
		return 'o';
	case 'p':
		return 'd';
	case 'q':
		return 'b';
	case 'r':
		return '\u0279';
	case 's':
		return 's';
	case 't':
		return '\u0287';
	case 'u':
		return 'n';
	case 'v':
		return '\u028C';
	case 'w':
		return '\u028D';
	case 'x':
		return 'x';
	case 'y':
		return '\u028E';
	case 'z':
		return 'z';
	case '[':
		return ']';
	case ']':
		return '[';
	case '(':
		return ')';
	case ')':
		return '(';
	case '{':
		return '}';
	case '}':
		return '{';
	case '?':
		return '\u00BF'; //From pne
	case '\u00BF':
		return '?';
	case '!':
		return '\u00A1';
	case "\'":
		return ',';
	case ',':
		return "\'";
	}
	return c;
}

//]]>
</script>
<script type="text/javascript" src="http://<? echo get_server_name().$globals['base_url']; ?>js/sneak14.js.php"></script>
<?


// Check the tab options and set corresponging JS variables
if ($current_user->user_id > 0) {
	if (!empty($_REQUEST['friends'])) {
		$taboption = 2;
		echo '<script type="text/javascript">global_options.show_friends = true;</script>';
	} elseif (!empty($_REQUEST['admin']) && $current_user->user_id > 0 && ($current_user->admin)) {
		$taboption = 3;
		echo '<script type="text/javascript">global_options.show_admin = true;</script>';
	}elseif (!empty($_REQUEST['devel']) && $current_user->user_id > 0 && ($current_user->devel)) {
		$taboption = 4;
		echo '<script type="text/javascript">global_options.show_devel = true;</script>';
	} else {
		$taboption = 1;
	}
	print_sneak_tabs($taboption);
}
//////


echo '<div class="sneaker" style="margin-top: 0px">';
echo '<div class="usneaker-legend fondo-caja redondo" onmouseout="tooltip.clear(event);" onmouseover="tooltip.clear(event);">';
echo '<form action="" class="usneaker-control" id="usneaker-control" name="usneaker-control">';
echo '<div class="cotillona-caja-larga">';

echo '<img id="play-pause-img" onclick="play_pause()" src="img/estructura/pixel.gif" alt="play/pause" title="play/pause" class="icon pause"/>&nbsp;&nbsp;';

echo '<input style="display: none;" type="checkbox" name="sneak-pubvotes" id="pubvotes-status" onclick="toggle_control(\'pubvotes\')" />';
echo '<input style="display: none;" type="checkbox" name="sneak-vote" id="vote-status" onclick="toggle_control(\'vote\')" />';
echo '<input style="display: none;" type="checkbox" name="sneak-problem" id="problem-status" onclick="toggle_control(\'problem\')" />';
echo '<input style="display: none;" type="checkbox" name="sneak-comment" id="comment-status" onclick="toggle_control(\'comment\')" />';
echo '<input style="display: none;" type="checkbox" name="sneak-new" id="new-status" onclick="toggle_control(\'new\')" />';
echo '<input style="display: none;" type="checkbox" name="sneak-published" id="published-status" onclick="toggle_control(\'published\')" />';


// Only registered users can see the chat messages

if ($current_user->user_id > 0) {
	$chat_checked = 'checked="checked"';
	echo '<input style="display: none;" type="checkbox" '.$chat_checked.' name="sneak-chat" id="chat-status" onclick="toggle_control(\'chat\')" />';
}

echo '<input style="display: none;" type="checkbox" checked="checked" name="sneak-post" id="post-status" onclick="toggle_control(\'post\')" />';
echo '<input style="display: none;" type="checkbox" name="sneak-encuesta" id="encuesta-status" onclick="toggle_control(\'encuesta\')" />';
echo _('cotillas').  ': <strong><span style="font-size: 120%;" id="ccnt">-</span></strong>&nbsp;&nbsp;';
echo '&nbsp;&nbsp;<abbr title="'._('tiempo en milisegundos para procesar cada petición a nuestro pobre microondas').'">ping-pong</abbr>: <strong><span style="font-size: 120%;" id="ping">---</span></strong>';
echo '</div>';
echo "</form>\n";



if ($current_user->user_id > 0){
	echo '<form name="chat_form" action="" onsubmit="return send_chat(this);">'; 
	echo '<select name="donde" id="donde">
	      <option value="" selected="selected">Todos</option>
	      <option value="@">Amigos</option>';
  
	if ($current_user->admin)
		echo '<option value="#">Il capos</option>';

	if ($current_user->devel)
		echo '<option value="%">Devels</option>';

	echo '</select>&nbsp;';
	echo '<input type="text" name="comment" id="comment-input" value="" style="width: 750px;" maxlength="230" autocomplete="off" />&nbsp;<span id="ttm"><input type="submit" value="'._('enviar').'" class="button"/></span>';
	echo '</form>';
}


echo '</div>' . "\n";

echo '<div id="singlewrap">' . "\n";

echo '<div class="usneaker-item">';
echo '<div class="usneaker-title">';
echo '<div class="usneaker-ts"><strong>'._('hora').'</strong></div>';
echo '<div class="usneaker-who">&nbsp;<strong>'._('mafioso').'</strong></div>';
echo '<div class="usneaker-story">&nbsp;<strong>'._('mensaje').'</strong></div>';
echo "</div>\n";
echo "</div>\n";


echo '<div id="items'.$i.'">';
for ($i=0; $i<$max_items;$i++) {
	echo '<div class="usneaker-item">&nbsp;</div>';
}

echo '</div><br/>';

/*if ($current_user->user_id > 0)
echo '<div id="radio"><object type="application/x-shockwave-flash" width="200px" height="15" data="radio.swf?playlist_url=http://radio.joneame.net:8000/radiojoneame.xspf&player_title=(( Radio Joneame )) Bienvenido - Ongi etorri - Benvingut - Welcome (( Radio Joneame ))">
<param name="movie" value="radio.swf?playlist_url=http://radio.joneame.net:8000/radiojoneame.xspf&player_title=(( Radio Joneame )) Bienvenido - Ongi etorri - Benvingut - Welcome (( Radio Joneame ))" />
</object></div>';*/

do_footer();

function print_sneak_tabs($option) {
	global $current_user;
	$active = array();
	$active[$option] = ' class="current"';
	echo '<ul class="tabmain">' . "\n";

	echo '<li'.$active[1].'><a href="'.$globals['base_url'].'ucotillona.php">'._('todos').'</a></li>' . "\n";
	echo '<li'.$active[2].'><a href="'.$globals['base_url'].'ucotillona.php?friends=1">'._('amigos').'</a></li>' . "\n";

	if ($current_user->user_id > 0 && $current_user->devel) {
	echo '<li'.$active[4].'><a href="'.$globals['base_url'].'ucotillona.php?devel=1">'._('devels').'</a></li>' . "\n";
	}

	if ($current_user->user_id > 0 && $current_user->admin) {
	echo '<li'.$active[3].'><a href="'.$globals['base_url'].'ucotillona.php?admin=1">'._('admin').'</a></li>' . "\n";
	}

	echo '<li style="margin-left: 10px;"><a href="'.$globals['base_url'].'telnet.php">'._('telnet').'</a></li>' . "\n";

	if ($_GET['hoygan'] == '1')
	  echo '<li><a href="'.$globals['base_url'].'ucotillona.php?hoygan=1"><em>'._('cotillhoygan').'</em></a></li>' . "\n";
	else
	  echo '<li><a href="'.$globals['base_url'].'ucotillona.php?hoygan=1">'._('cotillhoygan').'</a></li>' . "\n";

	if ($_GET['flip'] == '1')
	  echo '<li"><a href="'.$globals['base_url'].'ucotillona.php?flip=1"><em>'._('al revés').'</em></a></li>' . "\n";
	else
	  echo '<li><a href="'.$globals['base_url'].'ucotillona.php?flip=1">'._('al revés').'</a></li>' . "\n";

	echo '<li style="margin-left: 10px;"><a class="separada" href="'.$globals['base_url'].'cotillona.php">'._('maxi cotillona').'</a></li>' . "\n";

	echo '</ul>' . "\n";
}

?>
