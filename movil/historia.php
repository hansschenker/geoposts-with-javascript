<?
// The source code packaged with this file is Free Software, Copyright (C) 2008 by
// Ricardo Galli <gallir at uib dot es>.
// It's licensed under the AFFERO GENERAL PUBLIC LICENSE unless stated otherwise.
// You can get copies of the licenses here:
// 		http://www.affero.org/oagpl.html
// AFFERO GENERAL PUBLIC LICENSE is also included in the file called "COPYING".

include('config.php');
include(mnminclude.'link.php');
include(mnminclude.'comment.php');
include(mnminclude.'html1-mobile.php');

$globals['ads'] = true;
$globals['link'] = true;

$link = new Link;


if (!isset($_REQUEST['id']) && !empty($_SERVER['PATH_INFO'])) {
	$url_args = preg_split('/\/+/', $_SERVER['PATH_INFO']);
	array_shift($url_args); // The first element is always a "/"
	$link->uri = $db->escape($url_args[0]);
	if (! $link->read('uri') ) {
		not_found();
	}
} else {
	$url_args = preg_split('/\/+/', $_REQUEST['id']);
	$link->id=intval($url_args[0]);
	if(is_numeric($url_args[0]) && $link->read('id') ) {
		// Redirect to the right URL if the link has a "semantic" uri
		if (!empty($link->uri) && !empty($globals['base_story_url'])) {
			if (!empty($url_args[1])) $extra_url = '/' . urlencode($url_args[1]);
			header('Location: ' . $link->get_permalink(). $extra_url);
			die;
		}
	} else {
		not_found();
	}
}


if ($link->is_discarded()) {
	// Dont allow indexing of discarded links
	if ($globals['bot']) not_found();
} else {
	//Only shows ads in non discarded images
	$globals['ads'] = true;
}

$current_page = '';

// Check for a page number which has to come to the end, i.e. ?id=xxx/P or /story/uri/P
$last_arg = count($url_args)-1;
if ($last_arg > 0) {
	if ($url_args[$last_arg] > 0) {
		$requested_page = $current_page =  (int) $url_args[$last_arg];
		array_pop($url_args);
	}
}

$order_field = 'comment_order';
$limit = '';

if ($globals['comments_page_size'] && $link->comments > $globals['comments_page_size']*$globals['comments_page_threshold']) {
	if (!$current_page) $current_page = ceil($link->comments/$globals['comments_page_size']);
	$offset=($current_page-1)*$globals['comments_page_size'];
	$limit = "LIMIT $offset,".$globals['comments_page_size'];
} 


if ($_POST['process']=='newcomment') {
    $comment = new Comment;
    $new_comment_error = $comment->save_from_post($link);
}

// Set globals
$globals['link'] = $link;
$globals['link_id'] = $link->id;
$globals['link_permalink'] = $globals['link']->get_permalink();

// to avoid search engines penalisation
if ($link->status == 'discard') {
	$globals['noindex'] = true;
}

$globals['extra_js'] = array('ajax_com.js');

do_header($link->title, 'post');

do_tabs("main",_('historia'), true);

// Para el spinner
echo '<input type="hidden" name="cargando" id="cargando" value="1">';

echo '<div id="newswrap">'."\n";
$link->print_summary();


echo '<div class="comments">';


$comments = $db->get_col("SELECT SQL_CACHE comment_id FROM comments WHERE comment_link_id=$link->id ORDER BY $order_field $limit");
if ($comments) {
	echo '<ol class="comments-list">';
	$comment = new Comment;
	foreach($comments as $comment_id) {
		$comment->id=$comment_id;
		$comment->read();
		$comment->print_summary($link, 700, true);
		echo "\n";
	}

	
	echo '<div id="ajaxcomments"></div>';
	echo '<div style="visibility:hidden;" id="ajaxcontainer"></div>';
	echo "</ol>\n";
}



echo '</div>' . "\n";

if($link->date > $globals['now']-$globals['time_enabled_comments'] && $link->comments < $globals['max_comments'] && 
	$current_user->authenticated && 
	($current_user->user_karma > $globals['min_karma_for_comments'] || $current_user->user_id == $link->author)) {
        print_comment_form();
}


do_comment_pages($link->comments, $current_page);

echo '</div>';

$globals['tag_status'] = $globals['link']->status;
do_footer();

// Show the error if the comment couldn't be inserted
if (!empty($new_comment_error)) {
    echo '<script type="text/javascript">';
    echo '$(function(){alert(\''._('comentario no insertado'). ":  $new_comment_error".'\')});';
    echo '</script>';
}


function do_comment_pages($total, $current, $reverse = true) {
	global $globals, $link;

	if ( ! $globals['comments_page_size'] || $total <= $globals['comments_page_size']*$globals['comments_page_threshold']) return;
	
	$index_limit = 10;

	$query = $link->get_relative_permalink();

	$total_pages=ceil($total/$globals['comments_page_size']);
	if (! $current) {
		if ($reverse) $current = $total_pages;
		else $current = 1;
	}
	$start=max($current-intval($index_limit/2), 1);
	$end=$start+$index_limit-1;
	
	echo '<div class="pages">';

	if($current==1) {
		echo '<span class="barra semi-redondo nextprev">&#171; '._('anterior').'</span>';
	} else {
		$i = $current-1;
		echo '<a class="barra semi-redondo" href="'.get_comment_page_url($i, $total_pages, $query).'">&#171; '._('anterior').'</a>';
	}

	$dots_before = $dots_after = false;
	for ($i=1;$i<=$total_pages;$i++) {
		if($i==$current) {
			echo '<span class="barra semi-redondo current">'.$i.'</span>';
		} else {
			if ($total_pages < 7 || abs($i-$current) < 3 || $i < 3 || abs($i-$total_pages) < 2) {
				echo '<a class="barra semi-redondo" href="'.get_comment_page_url($i, $total_pages, $query).'" title="'._('ir a página')." $i".'">'.$i.'</a>';
			} else {
				if ($i<$current && !$dots_before) {
					$dots_before = true;
					echo '<span class="barra semi-redondo nextprev">...</span>';
				} elseif ($i>$current && !$dots_after) {
					$dots_after = true;
					echo '<span class="barra semi-redondo nextprev">...</span>';
				}
			}
		}
	}
	
	if($current<$total_pages) {
		$i = $current+1;
		echo '<a class="barra semi-redondo" href="'.get_comment_page_url($i, $total_pages, $query).'">'._('siguiente').' &#187;</a>';
	} else {
		echo '<span class="barra semi-redondo nextprev">'._('siguiente'). ' &#187;</span>';
	}
	echo "</div>\n";

}

function get_comment_page_url($i, $total, $query) {
	global $globals;
	if ($i == $total) return $query;
	else return $query.'/'.$i;
}

function print_comment_form() {
	global $link, $current_user, $globals;

	if (!$link->author > 0 && !$link->sent) return;
	
	echo '<div class="commentform">'."\n";
	echo '<form action="" method="post">'."\n";
	echo '<h4>'._('escribe un comentario').'</h4><fieldset class="fondo-caja">'."\n";
	
	echo '<div style="margin-top: 10px;"><textarea name="comment_content" id="comment" cols="40" rows="6"></textarea></div>'."\n";
	echo '<script src="/js/ajax_com.js" type="text/javascript"></script>';

	// Allow gods to put "admin" comments which does not allow votes
	if ($current_user->admin )
		echo '<div style="float: left; margin-left: 10px;">';
	else
		echo '<div style="display: none;">';

		echo '<input name="type" type="checkbox" value="admin" id="comentario-admin"/>&nbsp;<label for="comentario-admin">'._('comentario admin').'</strong></label>'."\n";
		echo '&nbsp;&nbsp;<input name="especial" type="checkbox" value="especial" id="comentario-especial"/>&nbsp;<label for="comentario-especial">'._('no mostrar mi nick').'</strong></label>'."\n";
		echo '</div>';
	echo '<br/><input type="button" class="button" name="submit" id="submit_com" value="'._('enviar comentario').'" onClick="submit_comment();"/>'."\n";

	echo '<img id="spinner" class="blank" src="'.$globals['base_url'].'img/estructura/pixel.gif" width="16" height="16"/>';

	echo '<br/><span id="error_com"></span>';

	echo '<input type="hidden" id="process" name="process" value="newcomment" />'."\n";
	echo '<input type="hidden" id="randkey" name="randkey" value="'.rand(1000000,100000000).'" />'."\n";
	echo '<input type="hidden" id="link_id" name="link_id" value="'.$link->id.'" />'."\n";
	echo '<input type="hidden" id="user_id" name="user_id" value="'.$current_user->user_id.'" />'."\n";
	echo '</fieldset>'."\n";
	echo '</form>'."\n";
	echo "</div>\n";


}

?>
