<?php
// The source code packaged with this file is Free Software, Copyright (C) 2005 by
// Ricardo Galli <gallir at uib dot es>.
// It's licensed under the AFFERO GENERAL PUBLIC LICENSE unless stated otherwise.
// You can get copies of the licenses here:
// 		http://www.affero.org/oagpl.html
// AFFERO GENERAL PUBLIC LICENSE is also included in the file called "COPYING".


@include mnminclude.'ads-credits-functions.php';

include_once(mnminclude.'post.php');

// Warning, it redirects to the content of the variable
if (!empty($globals['lounge'])) {
	header('Location: http://'.get_server_name().$globals['base_url'].$globals['lounge']);
	die;
}

$globals['start_time'] = microtime(true);

header('Content-type: text/html; charset=utf-8');
if ($current_user->user_id) {
	header('Cache-Control: private');
}

function do_tabs($tab_name, $tab_selected = false, $extra_tab = false) {
	global $globals;

	$reload_text = _('recargar');
	$active = ' class="current"';

	if ($tab_name == "main" ) {
		echo '<ul class="tabmain">';

		// url with parameters?
		if (!empty($_SERVER['QUERY_STRING']))
			$query = "?".htmlentities($_SERVER['QUERY_STRING']);

		// START STANDARD TABS
		// First the standard and always present tabs
		// published tab
		if ($tab_selected == 'published') {
			echo '<li '.$active.'><a href="'.$globals['base_url'].'" title="'.$reload_text.'">'._('portada').'</a></li>';
		} else {
			echo '<li><a  href="'.$globals['base_url'].'">'._('portada').'</a></li>';
		}


		// Most voted
		if ($tab_selected == 'popular') {
			echo '<li '.$active.'><a href="'.$globals['base_url'].'las_mejores.php" title="'.$reload_text.'">'._('populares').'</a></li>';
		} else {
			echo '<li><a href="'.$globals['base_url'].'las_mejores.php">'._('populares').'</a></li>';
		}

		// shake it
		if ($tab_selected == 'shakeit') {
			echo '<li '.$active.'><a href="'.$globals['base_url'].'jonealas.php" title="'.$reload_text.'">'._('jon&eacute;alas').'</a></li>';
		} else {
			echo '<li><a href="'.$globals['base_url'].'jonealas.php">'._('jon&eacute;alas').'</a></li>';
		}
		// END STANDARD TABS

		//Extra tab
		if ($extra_tab) {
			if ($globals['link_permalink']) $url = $globals['link_permalink'];
			else $url = htmlentities($_SERVER['REQUEST_URI']);
			echo '<li '.$active.'><a href="'.$url.'" title="'.$reload_text.'">'.$tab_selected.'</a></li>';
		}
		echo '</ul>' . "\n";
		echo '<div style="clear:left"></div>'; // Some browsers wrap the tabs
	}
}

function do_header($title, $id='home') {
	global $current_user, $dblang, $globals;

	//echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n";
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">' . "\n";
	echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="'.$dblang.'">' . "\n";
	echo '<head>' . "\n";
	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . "\n";
	echo '<meta name="ROBOTS" content="NOARCHIVE" />'."\n";
	echo '<meta name="viewport" content="width=320"/>' . "\n";
	echo "<title>$title</title>\n";

	do_css_includes();

	echo '<meta name="generator" content="meneame" />' . "\n";
	if (!empty($globals['noindex'])) {
		echo '<meta name="robots" content="noindex,follow"/>' . "\n";
	}
	if (!empty($globals['tags'])) {
		echo '<meta name="keywords" content="'.$globals['tags'].'" />' . "\n";
	}
	if (empty($globals['favicon'])) $globals['favicon'] = 'img/favicons/favicon-jnm.png';
	echo '<link rel="icon" href="'.$globals['base_url'].$globals['favicon'].'" type="image/x-icon"/>' . "\n";
	//echo '<link rel="apple-touch-icon" href="'.$globals['base_url'].'img/favicons/apple-touch-icon.png"/>' . "\n";

	do_js_includes();

	if ($globals['extra_head']) echo $globals['extra_head'];

	echo '</head>' . "\n";
	echo "<body id=\"$id\" ". $globals['body_args']. ">\n";

	echo '<div id="header">' . "\n";
	echo '<a href="'.$globals['base_url'].'" title="'._('inicio').'" id="logo">'._("jonéame").'</a>'."\n";
	echo '<ul id="headtools">';

 	//echo '<li><a href="'.$globals['base_url'].'search.php">'. _('buscar').'</a></li>';
	if($current_user->authenticated) {
  		echo '<li><a href="'.$globals['base_url'].'login.php?op=logout&amp;return='.urlencode($_SERVER['REQUEST_URI']).'">'. _('cerrar sesi&oacute;n').'</a></li>';
 		echo '<li class="noborder"><a href="'.get_user_uri($current_user->user_login).'" title="'.$current_user->user_login.'"><img src="'.get_avatar_url($current_user->user_id, $current_user->user_avatar, 20).'" width="15" height="15" alt="'.$current_user->user_login.'"/></a></li>';
	} else {
  		echo '<li class="noborder"><a href="'.$globals['base_url'].'login.php?return='.urlencode($_SERVER['REQUEST_URI']).'">'. _('iniciar sesi&oacute;n').'</a></li>';
	}


	echo '</ul>' . "\n";
	echo '</div>' . "\n";
	echo '<div id="container">'."\n";
	do_banner_top_mobile();
}

function do_css_includes() {
	global $globals;

	if ($globals['css_movil']) {
		echo '<link rel="stylesheet" type="text/css" media="screen" href="'.$globals['base_url'].$globals['css_movil'].'" />' . "\n";
	}

	echo '<link rel="stylesheet" type="text/css" media="screen" href="'.$globals['base_url'].'css/icons.css?'.filemtime('css/icons.css').'"/>'."";

	foreach ($globals['extra_css'] as $css) {
		echo '<link rel="stylesheet" type="text/css" media="screen" href="'.$globals['base_url'].'css/'.$css.'" />' . "\n";
	}
}

function do_js_includes() {
	global $globals;

	echo '<script type="text/javascript">var base_url="'.$globals['base_url'].'";mobile_version = true;</script>'."\n";
	echo '<script src="'.$globals['base_url'].'js/mobile02.js" type="text/javascript"></script>' . "\n";
	echo '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js" type="text/javascript">';

	do_js_from_array($globals['extra_js']);
	echo '<script type="text/javascript">if(top.location != self.location)top.location = self.location;'."\n";
	if ($globals['extra_js_text']) {
		 echo $globals['extra_js_text']."\n";
	}
	echo '</script>'."\n";
}

function do_js_from_array($array) {
	global $globals;

	foreach ($array as $js) {
		if (preg_match('/^http|^\//', $js)) {
			echo '<script src="'.$js.'" type="text/javascript"></script>' . "\n";
		} else {
			echo '<script src="'.$globals['base_url'].'js/'.$js.'" type="text/javascript"></script>' . "\n";
		}
	}
}

function do_footer($credits = true) {
	global $globals;

	echo "</div>\n";
	if($credits) @do_credits_mobile();
	do_js_from_array($globals['post_js']);

	// warn warn warn 
	// dont do stats of password recovering pages
	@include('ads/stats-mobile.inc');
		echo '<script type="text/javascript">';

	echo'  var _gaq = _gaq || [];';
	echo "  _gaq.push(['_setAccount', 'UA-27927706-1']);
	  _gaq.push(['_trackPageview']);";

	echo "  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>";
	printf("\n<!--Generated in %4.3f seconds-->\n", microtime(true) - $globals['start_time']);
	echo "</body></html>\n";
}

function do_footer_menu() {
	global $globals, $current_user;

}

function force_authentication() {
	global $current_user;

	if(!$current_user->authenticated) {
		header('Location: '.$globals['base_url'].'login.php?return='.$_SERVER['REQUEST_URI']);
		die;
	}
	return true;
}

function do_pages($total, $page_size=25, $margin = true, $mini = false) {
	global $db;

	if ($total > 0 && $total < $page_size) return;

	$index_limit = 5;

	$query=preg_replace('/page=[0-9]+/', '', $_SERVER['QUERY_STRING']);
	$query=preg_replace('/^&*(.*)&*$/', "$1", $query);
	if(!empty($query)) {
		$query = htmlspecialchars($query);
		$query = "&amp;$query";
	}
	
	$current = get_current_page();
	$total_pages=ceil($total/$page_size);
	$start=max($current-intval($index_limit/2), 1);
	$end=$start+$index_limit-1;
	echo "\n".'<!--html1:do_pages_start-->';

	if ($margin) {
		echo '<div class="pages-margin">';
	} elseif ($mini) {
		echo '<div class="pages-mini">';
	} else {
		echo '<div class="pages">';
	}

	if($current==1) {
		echo '<span class="barra semi-redondo nextprev">&#171; '._('anterior'). '</span>';
	} else {
		$i = $current-1;
		echo '<a class="barra semi-redondo" href="?page='.$i.$query.'">&#171; '._('anterior').'</a>';
	}
 if ($total_pages > 0) {
	if($start>1) {
		$i = 1;
		echo '<a class="barra semi-redondo" href="?page='.$i.$query.'" title="'._('ir a página')." $i".'">'.$i.'</a>';
		echo '<span class="barra semi-redondo current">...</span>';
	}
	for ($i=$start;$i<=$end && $i<= $total_pages;$i++) {
		if($i==$current) {
			echo '<span class="barra semi-redondo current">'.$i.'</span>';
		} else {
			echo '<a class="barra semi-redondo" href="?page='.$i.$query.'" title="'._('ir a la página')." $i".'">'.$i.'</a>';
		}
	}
	if($total_pages>$end) {
		$i = $total_pages;
		echo '<span class="barra semi-redondo current">...</span>';
		echo '<a class="barra semi-redondo" href="?page='.$i.$query.'" title="'._('ir a la página')." $i".'">'.$i.'</a>';
	}
  } else {
	if($current>2) {
            echo '<a class="barra semi-redondo" href="?page=1" title="'._('ir a página')." 1".'">1</a>';
           echo '<span class="barra semi-redondo current">...</span>';
        }
	echo '<span class="barra semi-redondo current">'.$current.'</span>';
  }
        
	 if($total < 0 || $current<$total_pages) {
		$i = $current+1;
		echo '<a class="barra semi-redondo" href="?page='.$i.$query.'">'._('siguiente').' &#187;</a>';
	} else {
		echo '<span class="barra semi-redondo nextprev">&#187; '._('siguiente'). '</span>';
	}
	echo '</div>'.'<!--html1:do_pages-->';

}

?>
